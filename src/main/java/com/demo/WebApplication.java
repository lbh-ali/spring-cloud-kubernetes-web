package com.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Import;

/**
 * @author 刘斌华
 * @date 2021年10月10日
 * @since 1.0.0
 */
@SpringBootApplication(scanBasePackages = {
        "com.demo.config",
        "com.demo.controller"
})
@EnableDiscoveryClient
@Import({AutoConfiguration.class})
public class WebApplication {
    
    public static void main(String[] args) {
        
        SpringApplication.run(WebApplication.class, args);
        
    }
    
}
