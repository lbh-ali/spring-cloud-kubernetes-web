package com.demo;

import com.demo.loadbalancer.VersionedServicesListSupplier;
import io.fabric8.kubernetes.client.KubernetesClient;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cloud.kubernetes.commons.discovery.KubernetesDiscoveryProperties;
import org.springframework.cloud.kubernetes.commons.loadbalancer.KubernetesServicesListSupplier;
import org.springframework.cloud.kubernetes.fabric8.loadbalancer.Fabric8ServiceInstanceMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;

/**
 * @author 刘斌华
 * @date 2021年10月17日
 * @since 1.0.0
 */
public class AutoConfiguration {
    
    @Bean
    @Primary
    @ConditionalOnProperty(name = "spring.cloud.kubernetes.loadbalancer.mode", havingValue = "SERVICE")
    public KubernetesServicesListSupplier kubernetesServicesListSupplier(Environment environment,
                                                                         KubernetesClient kubernetesClient, Fabric8ServiceInstanceMapper mapper,
                                                                         KubernetesDiscoveryProperties discoveryProperties) {
        
        return new VersionedServicesListSupplier(environment, kubernetesClient, mapper, discoveryProperties);
        
    }
    
}
