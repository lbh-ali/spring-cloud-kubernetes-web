package com.demo.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author 刘斌华
 * @date 2021年10月13日
 * @since 1.0.0
 */
@Configuration
@ConfigurationProperties(prefix = WebConfig.PREFIX)
public class WebConfig {
    
    public static final String PREFIX = "demo.web";
    
    private String name;
    private String desc;
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getDesc() {
        return desc;
    }
    
    public void setDesc(String desc) {
        this.desc = desc;
    }
    
}
