package com.demo.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author 刘斌华
 * @date 2021年10月13日
 * @since 1.0.0
 */
@Configuration
@ConfigurationProperties(prefix = WebSecretConfig.PREFIX)
public class WebSecretConfig {
    
    public static final String PREFIX = "demo.web.secret";
    
    private String username;
    private String password;
    
    public String getUsername() {
        return username;
    }
    
    public void setUsername(String username) {
        this.username = username;
    }
    
    public String getPassword() {
        return password;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }
    
}
