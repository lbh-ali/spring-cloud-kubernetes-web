package com.demo.controller;

import com.demo.config.WebConfig;
import com.demo.config.WebSecretConfig;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 测试
 *
 * @author 刘斌华
 * @date 2021年10月10日
 * @since 1.0.0
 */
@RestController
@RequestMapping("/test")
public class TestController {
    
    @Resource
    private WebConfig webConfig;
    @Resource
    private WebSecretConfig webSecretConfig;
    
    @Resource
    private DiscoveryClient discoveryClient;
    
    /**
     * 获取所有服务名称列表
     *
     * @return 服务名称列表
     * @author 刘斌华
     * @date 2021年10月10日
     * @since 1.0.0
     */
    @GetMapping("/service")
    public List<String> service() {
        
        return discoveryClient.getServices();
        
    }
    
    /**
     * 获取指定服务的实例列表
     *
     * @param name 服务名称
     * @return 实例列表
     * @author 刘斌华
     * @date 2021年10月10日
     * @since 1.0.0
     */
    @GetMapping("/service/{name}")
    public List<ServiceInstance> service(@PathVariable("name") String name) {
        
        return discoveryClient.getInstances(name);
        
    }
    
    /**
     * 获取配置信息
     *
     * @return 配置信息
     * @author 刘斌华
     * @date 2021年10月10日
     * @since 1.0.0
     */
    @GetMapping("/config")
    public Map<String, String> config() {
        
        Map<String, String> config = new HashMap<>(8);
        config.put("name", webConfig.getName());
        config.put("desc", webConfig.getDesc());
        config.put("username", webSecretConfig.getUsername());
        config.put("password", webSecretConfig.getPassword());
        
        return config;
        
    }
    
    /**
     * 获取接口文档
     *
     * @param request 请求参数
     * @return 返回结果
     * @author 刘斌华
     * @date 2021年10月10日
     * @since 1.0.0
     */
    @PutMapping("/api")
    public TestResponse api(@RequestBody @Validated TestRequest request) {
        
        return new TestResponse();
        
    }
    
    /**
     * 测试请求参数
     *
     * @author 刘斌华
     * @date 2021年10月10日
     * @since 1.0.0
     */
    public static class TestRequest {
        
        /**
         * 姓名
         */
        @NotBlank
        private String name;
        /**
         * 年龄
         */
        private int age;
        /**
         * 性别
         */
        private Integer gender;
        /**
         * 列表
         */
        @NotEmpty
        @Valid
        private List<TestNested> list;
    
        public String getName() {
            return name;
        }
    
        public void setName(String name) {
            this.name = name;
        }
    
        public int getAge() {
            return age;
        }
    
        public void setAge(int age) {
            this.age = age;
        }
    
        public Integer getGender() {
            return gender;
        }
    
        public void setGender(Integer gender) {
            this.gender = gender;
        }
    
        public List<TestNested> getList() {
            return list;
        }
    
        public void setList(List<TestNested> list) {
            this.list = list;
        }
    }
    
    /**
     * 测试返回结果
     *
     * @author 刘斌华
     * @date 2021年10月10日
     * @since 1.0.0
     */
    public static class TestResponse {
        
        /**
         * 姓名
         */
        private String name;
        /**
         * 年龄
         */
        private int age;
        /**
         * 性别
         */
        private Integer gender;
        /**
         * 列表
         */
        private List<TestNested> list;
        
        public String getName() {
            return name;
        }
        
        public void setName(String name) {
            this.name = name;
        }
        
        public int getAge() {
            return age;
        }
        
        public void setAge(int age) {
            this.age = age;
        }
        
        public Integer getGender() {
            return gender;
        }
        
        public void setGender(Integer gender) {
            this.gender = gender;
        }
        
        public List<TestNested> getList() {
            return list;
        }
        
        public void setList(List<TestNested> list) {
            this.list = list;
        }
    }
    
    /**
     * 测试内嵌
     *
     * @author 刘斌华
     * @date 2021年10月10日
     * @since 1.0.0
     */
    public static class TestNested {
    
        /**
         * 不为空
         */
        @NotNull
        private Long longValue;
    
        public Long getLongValue() {
            return longValue;
        }
    
        public void setLongValue(Long longValue) {
            this.longValue = longValue;
        }
    }
    
}
